DROP DATABASE knee_rehab;

CREATE DATABASE
       IF NOT EXISTS knee_rehab
       DEFAULT CHARACTER SET latin1
       DEFAULT COLLATE latin1_general_cs;

CREATE TABLE knee_rehab.users(
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(40) UNIQUE NOT NULL,
    user_type VARCHAR(20) NOT NULL
);

CREATE TABLE knee_rehab.passwords(
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    password VARCHAR(40) NOT NULL
);

CREATE TABLE knee_rehab.patients(
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    user_id INT UNSIGNED NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    name VARCHAR(40) NOT NULL,
    surname VARCHAR(20) NOT NULL,
    dni VARCHAR(20) UNIQUE NOT NULL
);

CREATE TABLE knee_rehab.entry(
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    patient_id INT UNSIGNED not null,
    FOREIGN KEY (patient_id) REFERENCES patients(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    start_time TIMESTAMP(6) not null default CURRENT_TIMESTAMP(6),
    end_time TIMESTAMP(6) not null default CURRENT_TIMESTAMP(6),
    observations TEXT
);

CREATE TABLE knee_rehab.data_dof(
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    entry_id INT UNSIGNED not null,
    FOREIGN KEY (entry_id) REFERENCES entry(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    dof LONGTEXT,
    s_frequency FLOAT UNSIGNED not null
);

CREATE TABLE knee_rehab.data_emg(
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    entry_id INT UNSIGNED not null,
    FOREIGN KEY (entry_id) REFERENCES entry(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    emg LONGTEXT,
    s_frequency FLOAT UNSIGNED not null
);

CREATE TABLE knee_rehab.data_temp(
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    entry_id INT UNSIGNED not null,
    FOREIGN KEY (entry_id) REFERENCES entry(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    temp LONGTEXT,
    s_frequency FLOAT UNSIGNED not null
);

CREATE TABLE knee_rehab.data_stretch(
    ID INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    entry_id INT UNSIGNED not null,
    FOREIGN KEY (entry_id) REFERENCES entry(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    stretch LONGTEXT,
    s_frequency FLOAT UNSIGNED not null
);

INSERT into knee_rehab.users (username, user_type) VALUES ("admin", "administrator");
INSERT into knee_rehab.passwords (user_id, password) VALUES (1, "admin");
