from flask import flash, redirect
from werkzeug.security import check_password_hash
from Server.storage.database_utils import *
from Server.models.model_in import PatientMetadataModelIn
from Server.models.model_out import DeviceDataModelOut
import json


def check_authentication(connection, user_name, password):
    """
    Retrieves data regarding user_name and password, and verifies their existence and matching

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type user_name: str
    :param user_name: user name

    :type password: str
    :param password: password entered

    :rtype: dict / str
    :return: Data regarding the result of the database search
    """

    user_data = check_user(connection, user_name)
    if user_data is None:
        return None
    else:
        authenticated = check_password_for_user(connection, user_data["ID"], password)
        if authenticated is None:
            return None
        else:
            return user_data


def check_user(connection, user_name):
    """
    Checks if user is valid

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type user_name: str
    :param user_name: user name

    :rtype: dict / None
    :return: the data in storage regarding the user_name, or None if not found
    """

    query = "SELECT * FROM users WHERE username=%s;"
    values = (user_name,)
    result = get_content_from_database(connection, query=query, values=values, output_as_dict=True)
    if len(result) > 0:
        return result[0]
    else:
        return None


def check_password_for_user(connection, user_id, password):
    """
    Checks if password is correct for the specified user

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type user_id: int
    :param user_id: id associated with user name

    :type password: str
    :param password: password entered

    :rtype: dict / None
    :return: the data in storage regarding the specified user id and password. None if not found
    """

    query = "SELECT * FROM passwords WHERE user_id=%s AND password=%s;"
    values = (user_id, password,)
    result = get_content_from_database(connection, query=query, values=values, output_as_dict=True)
    if len(result) > 0:
        return result[0]
    else:
        return None


def check_user_and_password(connection, username, password):
    """
    Checks if user and password exist and match

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type username: str
    :param username: user name

    :type password: str
    :param password: password

    :rtype: bool
    :return: Whether the user and password are correct and match
    """

    exists = check_user(connection, username)

    if exists is None:
        flash('Invalid username or password')
        return redirect('/login_form')

    return check_password_for_user(connection, exists["ID"], password)


def check_user_and_password_hash(connection, username, password_hash):
    exists = check_user(connection, username)

    if exists is None:
        flash('Invalid username or password')
        return False

    query = "SELECT * FROM passwords WHERE user_id=%s"
    values = (exists["ID"],)

    password = get_content_from_database(connection, query=query, only_first=True, values=values, output_as_dict=True)

    try:
        if not check_password_hash(password_hash, password[0]["password"]):
            flash('Invalid username or password')
            return False

        return True

    except Exception as e:
        print(e)
        return False


def store_new_user(connection, username, password):
    """
    Store new user and his/her password in database

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type username: str
    :param username: user name

    :type password: str
    :param password: password

    :rtype: bool
    :return: Whether the insertion was successful or not
    """

    try:
        query = generate_query_for_list("users", ["username", "user_type"], ["%s"] * 2)
        user_id = execute_query(connection, query, [username, "user"], True)

        query = generate_query_for_list("passwords", ["user_id", "password"], ["%s"] * 2)
        execute_query(connection, query, [user_id, password])
        return True

    except Exception as e:
        print(e)
        return False


def store_client_data(connection, data, patient_dni):
    """
    Store data from device in database

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type data: DeviceDataModelIn object
    :param data: data from the patient device

    :type patient_dni: str
    :param patient_dni: DNI of the patient

    :rtype: bool
    :return: Whether the insertion in the DB was successful or not
    """

    patient_metadata = check_patient(connection, PatientMetadataModelIn("", "", patient_dni))

    if patient_metadata is None:
        return False

    entry_query = generate_query_for_list("entry", ["patient_id", "start_time", "end_time", "observations"], ["%s"] * 4)
    entry_id = execute_query(connection, entry_query,
                             [patient_metadata["ID"], data.start_time, data.end_time, data.observations], True)

    try:
        for sensor in ["dof", "emg", "stretch", "temp"]:
            sensor_query = generate_query_for_list("data_" + sensor,
                                                   ["entry_id", sensor, "s_frequency"], ["%s"] * 3)
            execute_query(connection, sensor_query,
                          [entry_id, json.dumps(data.data[sensor]), data.frequencies[sensor]])

        return True

    except Exception as e:
        print(e)
        return False


def get_data_from_user(connection, user_id):
    """
    Checks if password is correct for the specified user

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type user_id: int
    :param user_id: id associated with user name

    :rtype: dict / None
    :return: the data in storage regarding the specified user id and password. None if not found
    """

    query = "SELECT * FROM entry WHERE user_id=%s;"
    values = (user_id,)
    result = get_content_from_database(connection, query=query, values=values, output_as_dict=True, only_first=False)
    if len(result) > 0:
        return result
    else:
        return None


def get_patients_for_user(connection, username):
    """
    Gets patient information for user

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type username: str
    :param username: user name

    :rtype: dict
    :return: retrieved information from clients for user
    """

    user_data = check_user(connection, username)

    query = "SELECT * FROM patients WHERE user_id=%s;"
    values = (user_data["ID"],)
    result = get_content_from_database(connection, query=query, values=values, output_as_dict=True, only_first=False)
    return result


def check_patient(connection, patient):
    """
    Checks if patient already exists in Database

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type patient: PatientMetadataModelIn
    :param patient: patient data

    :rtype: dict / None
    :return: retrieved information from patient. None if none were found with given characteristics
    """

    query = "SELECT * FROM patients WHERE dni=%s;"
    values = (patient.dni,)
    result = get_content_from_database(connection, query=query, values=values, output_as_dict=True)
    if len(result) > 0:
        return result[0]
    else:
        return None


def store_new_patient(connection, patient, user_id):
    """
    Stores new patient in database
    
    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type patient: PatientMetadataModelIn
    :param patient: patient data

    :type user_id: int
    :param user_id: user identifier

    :rtype: bool
    :return: whether the insertion was successful or not
    """

    try:
        query = generate_query_for_list("patients", ["user_id", "name", "surname", "dni"], ["%s"] * 4)
        execute_query(connection, query, [user_id, patient.name, patient.surname, patient.dni], False)

        return True
    except Exception as e:
        print(e)
        return False


def get_data_from_patient(connection, patient_id):
    """
    Gets all data for patient with given patient_id

    :type connection: MariaDB connection object
    :param connection: Connection object to MySQL database

    :type patient_id: int
    :param patient_id: identifier of patient in database

    :rtype: DeviceDataModelOut
    :return: data for the patient
    """

    query = "SELECT * from entry WHERE patient_id=%s;"
    values = (patient_id,)

    data = dict()

    entry = get_content_from_database(connection, query=query, values=values, output_as_dict=True, only_first=True)

    if len(entry) == 0:
        return []

    entry = entry[0]

    for sensor in ["dof", "emg", "stretch", "temp"]:
        sensor_query = "SELECT * FROM {} WHERE entry_id=%s;".format('data_' + sensor)
        sensor_values = (entry["ID"],)

        sensor_data = get_content_from_database(
            connection, query=sensor_query, values=sensor_values, output_as_dict=True)[0]

        data[sensor] = sensor_data

    modeled_data = DeviceDataModelOut(entry, data)

    return modeled_data
