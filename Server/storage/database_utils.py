import datetime
import mysql.connector as mariadb
from ast import literal_eval


class DatabaseConnection:
    def __init__(self, db_info=None):
        self.mariadb_connection = mariadb.connect(host=db_info['IP'], port=db_info['PORT'], user=db_info['USER'],
                                                  password=db_info['PASSWORD'], database=db_info['DATABASE'])

    def __enter__(self):
        return self.mariadb_connection

    def __exit__(self, type, value, traceback):
        self.mariadb_connection.close()


class WrappedInSqlFunction(str):
    pass


def from_list_to_database_format(data_list):
    """
    Converts a given list into database format (string).

    :type data_list: list
    :param data_list: the list to be converted to database format.

    :rtype: string
    :returns: the data in database format (string)

    """

    # TO STRING  ["high", "medium", "low"] -> "high medium low"
    database_format_data = ' '.join(data_list)
    return database_format_data


def from_database_format_to_list(data_database_format):
    """
    Converts a given data from database format (string) into a list.

    :type data_database_format: str
    :param data_database_format: the data on database format (string) to be converted to a list.

    :rtype: list
    :returns: the data in list format.

    """
    # TO LIST   "high medium low" ->  ["high", "medium", "low"]
    data_list = data_database_format.split(" ")
    return data_list


def wrap_in_sql_function(value, function_name):
    """
    Converts a given value into a WrappedInSqlFunction object.

    :type value: str
    :param value: the value to be wrapped as a function

    :type function_name: str
    :param function_name: name of the sql function that will wrap the given value

    :rtype: WrappedInSqlFunction object
    :returns: an object containing the value wrapped as an sql function

    """

    wrapped = WrappedInSqlFunction(value)
    wrapped.original = value
    wrapped.function = function_name
    return wrapped


def is_wrapped_in_sql_function(value):
    """
    Determines if a given value is a WrappedInSqlFunction object.

    :type value: str or WrappedInSqlFunction object
    :param value: element to check

    :rtype: bool
    :returns: whether or not the given value is an instance of the WrappedInSqlFunction class

    """

    return isinstance(value, WrappedInSqlFunction)


def right_now(date_format='%Y-%m-%d %H:%M:%S.%f', as_datetime=False):
    """
    Gets the current date in database format.

    :type date_format: str
    :param date_format: optional. If included the data is converted into that given timestamp format.

    :type as_datetime: bool
    :param as_datetime: optional. If set to True, the method will return a datetime.
                                  If it is False (default) it will return a string

    :rtype: str
    :returns: the specified date in database format.

    """
    if as_datetime:
        return datetime.datetime.now()

    return datetime.datetime.now().strftime(date_format)


def get_content_from_database(connection, table_name=None, query=None, values=None, only_first=True,
                              output_as_dict=False):
    """
    Method to do select queries to the database.

    :type connection: object
    :param connection: connection to the mariadb database

    :type table_name: str
    :param table_name: optional. If no query is specified, all the elements of the specified table will be retrieved.

    :type query: str
    :param query: optional. Query to be executed.

    :type values: tuple
    :param values: optional. Must be included if query is specified. Tuple with the values to replace the %s values.

    :type only_first: bool
    :param only_first: optional. If True (default) returns only the first result.
                        If False returns all the query results.

    :type output_as_dict: bool
    :param output_as_dict: optional. If False (default), returns a list.
                            If True, returns a dict with the colum names as keys.

    :rtype: list of dict, None, tuple
    :returns: if table_name is None, returns None.
            If output_as_dict is True and only_first is True, returns a list containing a dictionary with the columns
            and the first row.
            If output_as_dict is True and only_first is False, returns a list containing a dictionary with the columns
            and all the rows.
            If output_as_dict is False and only_first is True, returns a tuple containing the first row.
            If output_as_dict is False and only_first is False, returns a tuple containing all the rows.

    """

    cursor = connection.cursor(buffered=True)
    if query is None:
        if table_name is None:
            return None
        query = "SELECT * FROM {};".format(table_name)
        values = ()
    cursor.execute(query, values)
    if output_as_dict:
        columns = tuple([d[0] for d in cursor.description])
        result = []
        if only_first:
            row = cursor.fetchone()
            if row is not None:
                result.append(dict(zip(columns, row)))
        else:
            for row in cursor:
                result.append(dict(zip(columns, row)))
    else:
        result = cursor.fetchone() if only_first else cursor.fetchall()
    cursor.close()
    connection.commit()
    return result


def get_content_regarding_foreign_id(connection, this_table, foreign_table, this_field, foreign_field, where_clause,
                                     values, only_first=True, output_as_dict=False, return_vals="*"):
    """
    Allows performing select queries to a database table using the id of another table.

    :type connection: object
    :param connection: connection to the mariadb database

    :type this_table: str
    :param this_table: table where the data is going to be inserted

    :type foreign_table: str
    :param foreign_table: table we are going to perform the join with

    :type this_field: str
    :param this_field: name of the this_table field where the join is going to be performed

    :type foreign_field: str
    :param foreign_field: name of the foreign_table field where the join is going to be performed

    :type where_clause: str
    :param where_clause: string containing the where clause used to find the registers.

    :type values: str
    :param values: optional. Must be included if query is specified. Tuple with the values to replace the %s values.

    :type only_first: bool
    :param only_first: optional. If True (default) returns only the first result.
                        If False returns all the query results.

    :type output_as_dict: bool
    :param output_as_dict: optional. If False (default), returns a list.
                            If True, returns a dict with the colum names as keys.

    :type return_vals: str
    :param return_vals: optional. List with the values we want to return. If not specified, it will be *

    :rtype: list of dict, None, tuple
    :returns: if table_name is None, returns None.
            If output_as_dict is True and only_first is True, returns a list containing a dictionary with the columns
            and the first row.
            If output_as_dict is True and only_first is False, returns a list containing a dictionary with the columns
            and all the rows.
            If output_as_dict is False and only_first is True, returns a tuple containing the first row.
            If output_as_dict is False and only_first is False, returns a tuple containing all the rows.

    """

    query = """SELECT {return_vals} FROM {this_table}
                JOIN {foreign_table} ON {this_table}.{this_field}={foreign_table}.{foreign_field}
                WHERE {where_clause};""".format(
        return_vals=return_vals,
        this_table=this_table, foreign_table=foreign_table,
        this_field=this_field, foreign_field=foreign_field,
        where_clause=where_clause)

    return get_content_from_database(connection, query=query, values=values, only_first=only_first,
                                     output_as_dict=output_as_dict)


def insert_info_in_database(connection, table_name, values_names, values_types, values, tuple_values_names=None,
                            replace_on_conflict=False, return_id=False):
    """
    Inserts values into the database.

    :type connection: object
    :param connection: connection to the mariadb database

    :type table_name: str
    :param table_name: table where we want to insert the data.

    :type values_names: str
    :param values_names: string containing a tuple with column names. i.e.: "(task_name, creator_uuid, creation_time,
    modification_time)"

    :type values_types: str
    :param values_types: string containing a tuple with column types. i.e.: "(%s,%s,%s,%s)"

    :type values: tuple
    :param values: tuple of the values to insert

    :type tuple_values_names: tuple
    :param tuple_values_names: optional. Must be included if replace_on_conflict is True.
                               Tuple containing the names of the columns.

    :type replace_on_conflict: bool
    :param replace_on_conflict: optional. If False (default) leaves the current value in the table.
                                If True replaces with the new value in case of conflict.

    :type return_id: bool
    :param return_id: whether to return the id of the element inserted or not

    :rtype: None
    :returns: None
    """

    values = list(values)
    if replace_on_conflict:
        map_values_string, map_values_values = get_map_values(tuple_values_names, values)
        values += map_values_values

        query = """INSERT INTO {table_name} {values_names} 
            VALUES {values_types}
            ON DUPLICATE KEY UPDATE {map_values}""".format(table_name=table_name, values_names=values_names,
                                                           values_types=values_types, map_values=map_values_string)

    else:
        query = "INSERT IGNORE INTO {table_name} {values_names} VALUES {values_types}" \
            .format(table_name=table_name, values_names=values_names, values_types=values_types)

    return execute_query(connection, query, values, return_id)


def generate_query_for_list(table_name, labels, values):
    """
    Generate the query strings for sending the lists of values and labels to the specified table

    :type table_name: str
    :param table_name: the target table name in MySQL

    :type labels: list<str>
    :param labels: the labels of all the subsequent values in the MySQL table.

    :type values: list<str>
    :param values: the list of the values corresponding to the previous labels.

    :rtype: str
    :return: the query string to insert all the data into 'table'.
    """

    allowed_values = ("%s",)

    for value in values:
        if value not in allowed_values:
            raise ValueError("Value not allowed")

    return "INSERT INTO %s (" % table_name + ', '.join(labels) + ") VALUES (" + ', '.join(values) + ")"


def generate_query_for_matrix(table, labels, values):
    """
    Generate the query strings for sending a list of lists of values and labels to the specified table

    :type table: str
    :param table: the target table name in MySQL

    :type labels: list<n x str>
    :param labels: the 'n' labels of all the subsequent values in the MySQL table.

    :type values: list< n x list< m x str> >
    :param values: the 'n'-long list of all 'm'-long lists of the values corresponding to the previous 'n' labels.
        All 'n' sub-list within must be the same length 'm'. Each of the 'n' lists corresponds to all the values
        to be inserted in a specific label entry.

    :rtype: list< n x str>
    :return: a list of all the query strings to insert all the data into 'table'.
    """

    r_list = []

    # Transpose the pseudo-matrix of values, so that row 'i' contains all the values corresponding to labels[i]
    values = list(map(list, zip(*values)))

    # Generate all the query strings for MySQL
    for row in values:
        r_list.append((generate_query_for_list(table, labels, ["%s"] * len(row)), row))
    return r_list


def insert_element_regarding_foreign_id(connection, this_table, values_names, values_types, values, foreign_table,
                                        foreign_where_id, tuple_values_names=None, replace_on_conflict=False):
    """
    Inserts values into the database using the id of another table.

    :type connection: object
    :param connection: connection to the mariadb database

    :type this_table: str
    :param this_table: table where the data is going to be inserted

    :type values_names: str
    :param values_names: string containing a tuple with column names. i.e.: "(task_name, creator_uuid, creation_time,
    modification_time)"

    :type values_types: str
    :param values_types: string containing a tuple with column types, EXCEPT the first one which is included with the
    subquery. i.e.: ",%s,%s,%s"

    :type values: tuple
    :param values: tuple of the values to insert

    :type foreign_table: str
    :param foreign_table: name of the table the subquery will be executed on

    :type foreign_where_id: str
    :param foreign_where_id: value to use in the subquery where clause (i.e. in "WHERE task_name='task1'",
    foreign_where_id would be "task_name")

    :type tuple_values_names: str
    :param tuple_values_names: optional. Must be included if replace_on_conflict is True.
                               Tuple containing the names of the columns.

    :type replace_on_conflict: bool
    :param replace_on_conflict: optional. If False (default) leaves the current value in the table.
                                If True replaces with the new value in case of conflict.
    """

    query = """INSERT IGNORE INTO {table_name} {values_names} 
                VALUES ((SELECT ID from {foreign_table_name} where {foreign_where_id}=%s), {values_types})""".format(
        table_name=this_table, values_types=values_types, values_names=values_names,
        foreign_table_name=foreign_table, foreign_where_id=foreign_where_id)
    values = list(values)
    if replace_on_conflict:
        map_values_string, map_values_values = get_map_values(tuple_values_names, values)
        values += map_values_values
        query = """{query} ON DUPLICATE KEY UPDATE {map_values}""".format(query=query, map_values=map_values_string)

    return execute_query(connection, query, values, return_id=True)


def insert_with_query(connection, query, values, map_values=None, replace_on_conflict=False):
    """

    :type connection: object
    :param connection: connection to the mariadb database

    :type query: str
    :param query: query to execute

    :type values: str tuple
    :param values: tuple of the values to insert

    :type map_values: tuple
    :param map_values: optional. Must be included if replace_on_conflict is True.
                       Values to update in case of conflict. It is None as default.

    :type replace_on_conflict: bool
    :param replace_on_conflict: optional. If False (default) leaves the current value in the table.
                                If True replaces with the new value in case of conflict.
    """

    if replace_on_conflict:
        query = """{query} ON DUPLICATE KEY UPDATE {map_values}""".format(query=query, map_values=map_values)
    execute_query(connection, query, values)


def update_field_in_database(connection, table_name, set_clause, where_clause, where_values):
    """
    Updates a specific table.

    :type connection: object
    :param connection: connection to the mariadb database

    :type table_name: str
    :param table_name: table where the data is going to be updated

    :type set_clause: str
    :param set_clause: string with the values to update. i.e.: "uuid='90a8sd0f9asd0f80', creator='a0sdf8asdf098a90sdf"

    :type where_clause: str
    :param where_clause: string with the values to locate the register.  i.e.: "task_name=%s AND lesson_name LIKE %s"

    :type where_values: tuple
    :param where_values: tuple with the values to locate the register. i.e.: ('task1','lesson1')

    """

    query = """UPDATE {table_name} 
        SET {set_clause}
        WHERE {where_clause};""".format(
        table_name=table_name,
        set_clause=set_clause,
        where_clause=where_clause)

    print("query = {}".format(query))
    print("values = {}".format(where_values))
    result = execute_query(connection, query=query, values=where_values)


def update_field_regarding_foreign_id(connection, this_table, foreign_table, this_field, foreign_field, set_clause,
                                      where_clause, values):
    """
    Updates a database using the id of another table.

    :type connection: object
    :param connection: connection to the mariadb database

    :type this_table: str
    :param this_table: table where the data is going to be updated

    :type foreign_table: str
    :param foreign_table: name of the foreign_table field where the join is going to be performed

    :type this_field: str
    :param this_field: name of the this_table field where the join is going to be performed

    :type foreign_field: str
    :param foreign_field: name of the foreign_table field where the join is going to be performed

    :type set_clause: str
    :param set_clause: string with the values to update. i.e.: "uuid='90a8sd0f9asd0f80', creator='a0sdf8asdf098a90sdf"

    :type where_clause: str
    :param where_clause: string with the values to locate the register.  i.e.: "task_name=%s AND lesson_name LIKE %s"

    :type values: tuple
    :param values: tuple with the values to locate the register. i.e.: ('task1','lesson1')

    """

    query = """UPDATE {this_table} 
                JOIN {foreign_table} ON {this_table}.{this_field}={foreign_table}.{foreign_field} 
                SET {set_clause}
                WHERE {where_clause};""".format(
        this_table=this_table, foreign_table=foreign_table,
        this_field=this_field, foreign_field=foreign_field,
        set_clause=set_clause, where_clause=where_clause)
    result = execute_query(connection, query=query, values=values)


def delete_register_from_database(connection, table_name, where_clause, where_values):
    """
    Deletes a register from a specified table.

    :type connection: object
    :param connection: connection to the mariadb database

    :type table_name: str
    :param table_name: table where we are going to delete data from

    :type where_clause: str
    :param where_clause: string with the values to locate the register. i.e.: "task_name=%s AND lesson_name LIKE %s"

    :type where_values: str
    :param where_values: tuple with the values to locate the register. i.e.: ('task1','lesson1')

    """

    query = "DELETE FROM {table_name} WHERE {where_clause};".format(
        table_name=table_name,
        where_clause=where_clause)
    result = execute_query(connection, query=query, values=where_values)


def delete_register_regarding_foreign_id(connection, this_table, foreign_table, this_field, foreign_field, where_clause,
                                         values):
    """
    Deletes a register using the id of another table.

    :type connection: object
    :param connection: connection to the mariadb database

    :type this_table: str
    :param this_table: table where the data is going to be deleted.

    :type foreign_table: str
    :param foreign_table: name of the foreign_table field where the join is going to be performed

    :type this_field: str
    :param this_field: name of the this_table field where the join is going to be performed

    :type foreign_field: str
    :param foreign_field: name of the foreign_table field where the join is going to be performed

    :type where_clause: str
    :param where_clause: string with the values to locate the register.  i.e.: "task_name=%s AND lesson_name LIKE %s"

    :type values: str
    :param values: tuple with the values to locate the register. i.e.: ('task1','lesson1')
    """

    query = """DELETE {this_table} FROM {this_table} 
                JOIN {foreign_table} ON {this_table}.{this_field}={foreign_table}.{foreign_field} 
                WHERE {where_clause};""".format(
        this_table=this_table, foreign_table=foreign_table,
        this_field=this_field, foreign_field=foreign_field,
        where_clause=where_clause)

    result = execute_query(connection, query=query, values=values)


def execute_query(mariadb_connection, query, values=(), return_id=False):
    """
    Method to execute an specified query in the database.

    :type mariadb_connection:  object
    :param mariadb_connection: connection to the mariadb database

    :type query: str
    :param query: query to execute

    :type values: list
    :param values: optional. If included, executes the query with that given values.

    :type return_id: bool
    :param return_id: optional. If included, the method will return the last inserted id

    :rtype: int
    :returns: if requested in the return_id param, return the id of the last row of the table

    """

    cursor = mariadb_connection.cursor(buffered=True)
    result = cursor.execute(query, values)
    serial_query = cursor.lastrowid

    mariadb_connection.commit()
    cursor.close()

    if return_id:
        print("-- lastrowid = {}".format(serial_query))
        return serial_query

    return result


def get_map_values(values_names, values):
    """
    Transforms a tuple of values into dictionary format.

    :type values_names: tuple
    :param values_names: tuple containing the dictionary keys

    :type values: tuple
    :param values: tuple containing the dictionary values

    :rtype: dict
    :returns: a dictionary containing the values associated to their corresponding keys
    """

    if type(values_names) == str:
        values_names = literal_eval(values_names)

    map_values_strings = []
    map_values_values = []
    for k, v in zip(values_names, values):
        if is_wrapped_in_sql_function(k):
            map_values_strings.append("{k}={function}(%s)"
                                      .format(k=k.original,
                                              function=k.function))
        else:
            map_values_strings.append("{k}=%s".format(k=k))

        map_values_values.append(v)

    return ', '.join(map_values_strings), map_values_values


def simple_get(connection, in_value, in_field, out_field, table, only_first=True):
    """
    Returns the out_field of a in_field in the table table
    :param connection:
    :param in_value: value of the in_field
    :param in_field:
    :param out_field:
    :param table:
    :param only_first
    :return:
    """

    query = """(SELECT {out_field} FROM {table} WHERE {in_field}=%s)""".format(
        table=table, out_field=out_field, in_field=in_field
    )
    values = (in_value,)
    results = get_content_from_database(connection, query=query, values=values, only_first=only_first,
                                        output_as_dict=True)
    if only_first:
        return results[0][out_field]
    else:
        return [x[out_field] for x in results]
