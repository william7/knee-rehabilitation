from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import InputRequired, EqualTo


class RegisterForm(FlaskForm):
    username = StringField(u'Username', [InputRequired()])
    password = PasswordField(u'Password', [InputRequired(), EqualTo('repeat_password', message='Passwords must match')])
    repeat_password = PasswordField(u'Repeat password')


class LoginForm(FlaskForm):
    username = StringField(u'Username', [InputRequired()])
    password = PasswordField(u'Password', [InputRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Enter')


class EnterPatientForm(FlaskForm):
    name = StringField('Patient name', [InputRequired()])
    surname = StringField('Patient surname', [InputRequired()])
    dni = StringField('DNI', [InputRequired()])
    submit = SubmitField('Enter patient')
