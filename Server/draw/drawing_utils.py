import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import tempfile
import numpy as np
from base64 import b64encode
from datetime import datetime


def create_plot_img(data, initial_time, final_time, title, y_label):

    x_values = generate_x_axis(initial_time, final_time, len(data))

    tick_spacing = len(data)/10

    fig, ax = plt.subplots(1, 1, figsize=(24, 6))
    ax.plot(x_values, data)
    ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))

    plt.xlabel('Time')
    plt.ylabel(y_label)
    plt.title(title)

    with tempfile.TemporaryFile(suffix=".png") as temp_file:
        fig.savefig(temp_file, format="png", bbox_inches='tight')
        temp_file.seek(0)
        return b64encode(temp_file.read())


def generate_x_axis(initial, final, n_samples):

    time_step = (final.timestamp() - initial.timestamp())/n_samples

    timestamp_vec = np.arange(initial.timestamp(), final.timestamp(), time_step).tolist()

    while n_samples < len(timestamp_vec):
        timestamp_vec.pop(-1)

    for i in range(0, len(timestamp_vec)):
        timestamp_vec[i] = datetime.fromtimestamp(timestamp_vec[i]).strftime("%Y-%m-%d %H:%M:%S")

    return timestamp_vec
