import datetime


def to_date(timestamp):
    if type(timestamp) == float:
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S.%f')

