import json


class DeviceDataModelOut:
    def __init__(self, entry, data):
        self.start_time, self.end_time, self.observations = DeviceDataModelOut.parse_entry(entry)
        self.data, self.frequencies = DeviceDataModelOut.parse_data(data)

        super(DeviceDataModelOut, self).__init__()

    @staticmethod
    def parse_entry(entry):
        return entry["start_time"], entry["end_time"], entry["observations"]

    @staticmethod
    def parse_data(client_data):
        return (dict([
                    ("dof", json.loads(client_data["dof"]["dof"])),
                    ("emg", json.loads(client_data["emg"]["emg"])),
                    ("stretch", json.loads(client_data["stretch"]["stretch"])),
                    ("temp", json.loads(client_data["temp"]["temp"]))]),
                dict([
                    ("dof", client_data["dof"]["s_frequency"]),
                    ("emg", client_data["emg"]["s_frequency"]),
                    ("stretch", client_data["stretch"]["s_frequency"]),
                    ("temp", client_data["temp"]["s_frequency"])
                ]))
