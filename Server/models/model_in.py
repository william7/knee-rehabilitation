from Server.utils.date_utils import to_date


class DeviceDataModelIn:
    def __init__(self, client_data):
        self.start_time, self.end_time, self.frequencies,\
            self.observations, self.data = DeviceDataModelIn.parse_data(client_data)
        super(DeviceDataModelIn, self).__init__()

    @staticmethod
    def parse_data(client_data):
        return (to_date(client_data["start_time"]), to_date(client_data["end_time"]), client_data["frequencies"],
                client_data["observations"], dict([
                    ("dof", client_data["dof"]),
                    ("emg", client_data["emg"]),
                    ("stretch", client_data["stretch"]),
                    ("temp", client_data["temp"])]))


class PatientMetadataModelIn:
    def __init__(self, patient_name, patient_surname, patient_dni):
        self.name = patient_name
        self.surname = patient_surname
        self.dni = patient_dni
