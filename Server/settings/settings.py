import os


class Settings:

    DATABASE = {
        "IP": os.getenv("KNOWLEDGE_DATABASE_HOST", "localhost"),
        "PORT": os.getenv("KNOWLEDGE_DATABASE_PORT", "3306"),
        "USER": os.getenv("KNOWLEDGE_DATABASE_USER", "root"),
        "PASSWORD": os.getenv("KNOWLEDGE_DATABASE_PASSWORD", "admin"),
        "DATABASE": os.getenv("KNOWLEDGE_DATABASE_NAME", "knee_rehab"),
    }
