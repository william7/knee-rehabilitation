from flask import Flask, request, render_template
from flask.json import jsonify
from werkzeug.security import generate_password_hash

from Server.settings import settings as settings
from Server.storage.database_logic import *
from Server.storage.database_utils import DatabaseConnection
from Server.models.model_in import DeviceDataModelIn, PatientMetadataModelIn
from Server.static.templates import LoginForm, RegisterForm, EnterPatientForm
from Server.draw.drawing_utils import create_plot_img

app = Flask(__name__)
app.config.update(dict(
    SECRET_KEY="powerful secretkey",
    WTF_CSRF_SECRET_KEY="a csrf secret key"
))


@app.route('/send_data/<patient_dni>', methods=["POST"])
def send_data(patient_dni):

    connection = DatabaseConnection(settings.Settings.DATABASE).mariadb_connection

    client_data = DeviceDataModelIn(json.loads(request.data.decode("utf-8")))
    status = store_client_data(connection, client_data, patient_dni)
    return str(status)


@app.route('/get_data/<user_name>/<password>.json', methods=["GET"])
def get_data(user_name, password):

    connection = DatabaseConnection(settings.Settings.DATABASE).mariadb_connection
    user_data = check_authentication(connection, user_name, password)

    if user_data is not None:
        recovered_data = get_data_from_user(connection, user_data["ID"])
        return jsonify(recovered_data)

    else:
        return "Invalid password or user"


@app.route('/login_form', methods=["GET", "POST"])
def register_new_user():
    form = LoginForm()
    if form.validate_on_submit():
        user = form.username.data
        password = form.password.data

        connection = DatabaseConnection(settings.Settings.DATABASE).mariadb_connection

        authenticated = check_user_and_password(connection, user, password)

        if authenticated is None:
            flash('Invalid username or password')
            return redirect('/login_form')

        return redirect('user_area/' + user + '/' + generate_password_hash(password))

    return render_template('login.html', title="Log In", form=form)


@app.route('/user_area/<username>/<password_hash>', methods=["GET", "POST"])
def enter_user_area(username, password_hash):

    connection = DatabaseConnection(settings.Settings.DATABASE).mariadb_connection
    authenticated = check_user_and_password_hash(connection, username, password_hash)

    if not authenticated:
        return redirect('/login_form')

    patients = get_patients_for_user(connection, username)

    name_list = []
    surname_list = []
    dni_list = []

    for patient in patients:
        name_list.append(patient["name"])
        surname_list.append(patient["surname"])
        dni_list.append(patient["dni"])

    if request.method == 'POST':
        if request.form['submit'] == 'Add new patient':
            return redirect('add_patient/' + username + '/' + password_hash)

    if request.method == 'GET':
        return render_template('user_area.html', nameList=name_list, surnameList=surname_list,
                               dniList=dni_list, indexList=range(0, len(name_list)))


@app.route('/add_patient/<username>/<password_hash>', methods=["GET", "POST"])
def add_new_patient_for_user(username, password_hash):

    connection = DatabaseConnection(settings.Settings.DATABASE).mariadb_connection
    authenticated = check_user_and_password_hash(connection, username, password_hash)

    if not authenticated:
        return redirect('/login_form')

    form = EnterPatientForm()
    if form.validate_on_submit():
        form = EnterPatientForm()
        patient = PatientMetadataModelIn(form.name.data, form.surname.data, form.dni.data)

        connection = DatabaseConnection(settings.Settings.DATABASE).mariadb_connection

        # Check patient is not repeated
        exists = check_patient(connection, patient)

        if exists is not None:
            flash('Patient already exists')
            return redirect('/add_patient/' + username + '/' + password_hash)

        user_data = check_user(connection, username)
        store_new_patient(connection, patient, user_data["ID"])
        return redirect('/user_area/' + username + '/' + password_hash)

    return render_template('register_patient.html', title="Register new patient", form=form)


@app.route('/new_user', methods=["GET", "POST"])
def enter_new_user_form():
    form = RegisterForm()
    if form.validate_on_submit():
        user = form.username.data
        password = form.password.data

        connection = DatabaseConnection(settings.Settings.DATABASE).mariadb_connection

        # Check user is not repeated
        exists = check_user(connection, user)

        if user is None or exists is not None:
            flash('Invalid username or password')
            return redirect('/new_user')

        store_new_user(connection, user, password)

        return redirect('/login_form')

    return render_template('register.html', title='Sign In', form=form)


@app.route('/user_area/<username>/view_patient/<patient_dni>/<password_hash>', methods=["GET"])
def visualize_patient_data(username, patient_dni, password_hash):

    connection = DatabaseConnection(settings.Settings.DATABASE).mariadb_connection
    authenticated = check_user_and_password_hash(connection, username, password_hash)

    if not authenticated:
        return redirect('/login_form')

    patient_metadata = check_patient(connection, PatientMetadataModelIn("", "", patient_dni))
    data = get_data_from_patient(connection, patient_metadata["ID"])

    if type(data) == list:
        return render_template('no_patient_data.html')

    b_image = create_plot_img(data.data["temp"], data.start_time, data.end_time, "Temperature", "ºC").decode("utf-8")

    return render_template('image_template.html', image=b_image)


@app.route('/')
def main():
    return redirect('/login_form')


if __name__ == "__main__":
    app.run()
