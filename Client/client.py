import datetime
import json
import random
import time
import requests
from scipy.io import loadmat

from Client.utils.requests_utils import get_client_direction


def send_data():

    dof_data = generate_fake_dof(3000)
    emg_data = generate_fake_emg(100000)
    stretch_data = generate_fake_stretch(300)
    temp_data = generate_fake_temp(300)

    data = dict([
        ('dof', dof_data), ('emg', emg_data), ('stretch', stretch_data), ('temp', temp_data),
        ('start_time', time.time()), ('end_time', time.time() + datetime.timedelta(hours=1).total_seconds(),),
        ('observations', 'normal'), ('frequencies',
                                     dict([('dof', 30), ('emg', 5000), ('stretch', 10), ('temp', 10)]))
        ])

    requests.post(url=get_client_direction(), data=json.dumps(data))


def generate_fake_dof(n_samples):
    array = [0]*n_samples
    for index in range(0, n_samples):
        array[index] = random.uniform(-180, 180)
    return array


def generate_fake_emg(n_samples):
    array = [0]*n_samples
    for index in range(0, n_samples):
        array[index] = random.uniform(-10000, 10000)
    return array


def generate_fake_stretch(n_samples):
    array = [0]*n_samples
    for index in range(0, n_samples):
        array[index] = random.uniform(0, 100)
    return array


def generate_fake_temp(n_samples):

    x = loadmat('/home/guillem/Downloads/Senyals/temp.mat')
    temperature = x['temperatura'][1].tolist()

    array = [0]*n_samples
    for index in range(0, n_samples):
        array[index] = random.uniform(20, 40)
    return temperature


if __name__ == "__main__":
    send_data()
