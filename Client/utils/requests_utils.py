from Client.settings.settings import Settings as Settings


def get_client_direction():
    return "{http}://{ip}:{port}/send_data/{dni}".format(
        http=Settings.SERVER_ROUTE["HTTP"],
        ip=Settings.SERVER_ROUTE["IP"],
        port=Settings.SERVER_ROUTE["PORT"],
        dni="514133112")
