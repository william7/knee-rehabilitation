import os


class Settings:

    SERVER_ROUTE = {
        "HTTP": os.getenv("KNOWLEDGE_SERVER_HTTP", "http"),
        "IP": os.getenv("KNOWLEDGE_SERVER_HOST", "localhost"),
        "PORT": os.getenv("KNOWLEDGE_SERVER_PORT", "5000"),
    }
