# Knee rehabilitation

This is a repository of the proof-of-concept knee rehabilitation web server and database structure

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them


```
sudo pip install -r requirements.txt
```

